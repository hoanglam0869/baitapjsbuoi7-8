var numsArray = [];

function addNumber() {
  var num = document.getElementById("num").value * 1;
  /* for (var i = 0; i < num; i++) {
    var rnd = getRndInteger(-20, 20);
    numsArray.push(rnd);
  } */
  numsArray.push(num);
  document.getElementById("result").innerText = numsArray;
}

function sumOfPositiveNumbers() {
  var sum = 0;
  numsArray.forEach((num) => {
    if (num > 0) {
      sum += num;
    }
  });
  document.getElementById("result-1").innerText = `Tổng số dương: ${sum}`;
}

function countPositiveNumbers() {
  var count = 0;
  numsArray.forEach((num) => {
    if (num > 0) {
      count++;
    }
  });
  document.getElementById("result-2").innerText = `Số dương: ${count}`;
}

function lowestNumber() {
  var lowest = numsArray[0];
  numsArray.forEach((num) => {
    if (num < lowest) {
      lowest = num;
    }
  });
  document.getElementById("result-3").innerText = `Số nhỏ nhất: ${lowest}`;
}

function lowestPositiveNumber() {
  var lowest = 0;
  var isFirstPositiveNumber = false;
  numsArray.forEach((num) => {
    if (num > 0) {
      if (!isFirstPositiveNumber) {
        lowest = num;
        isFirstPositiveNumber = true;
      }
      if (num < lowest) {
        lowest = num;
      }
    }
  });
  if (lowest == 0) {
    document.getElementById(
      "result-4"
    ).innerText = `Không có số dương trong mảng`;
  } else {
    document.getElementById(
      "result-4"
    ).innerText = `Số dương nhỏ nhất: ${lowest}`;
  }
}

function finalEvenNumber() {
  var finalEvenNumber = 0;
  numsArray.forEach((num) => {
    if (num % 2 == 0) {
      finalEvenNumber = num;
    }
  });
  document.getElementById(
    "result-5"
  ).innerText = `Số chẵn cuối cùng: ${finalEvenNumber}`;
}

function exchangePosition() {
  var pos1 = document.getElementById("pos1").value * 1;
  var pos2 = document.getElementById("pos2").value * 1;
  if (pos1 >= numsArray.length || pos2 >= numsArray.length) {
    document.getElementById("result-6").innerText = "Mảng sau khi đổi:";
    return;
  }
  var temp = numsArray[pos1];
  numsArray[pos1] = numsArray[pos2];
  numsArray[pos2] = temp;
  document.getElementById(
    "result-6"
  ).innerText = `Mảng sau khi đổi: ${numsArray}`;
}

function sortAscending() {
  for (var i = 0; i < numsArray.length - 1; i++) {
    for (var j = i + 1; j < numsArray.length; j++) {
      if (numsArray[i] > numsArray[j]) {
        var temp = numsArray[i];
        numsArray[i] = numsArray[j];
        numsArray[j] = temp;
      }
    }
  }
  document.getElementById(
    "result-7"
  ).innerText = `Mảng sau khi sắp xếp: ${numsArray}`;
}

function firstPrimeNumber() {
  var firstPrimeNumber = -1;
  for (var i = 0; i < numsArray.length; i++) {
    const num = numsArray[i];
    if (isPrimeNumber(num)) {
      firstPrimeNumber = num;
      break;
    }
  }
  document.getElementById("result-8").innerText = firstPrimeNumber;
}

function isPrimeNumber(num) {
  if (num < 1) return false;
  if (num == 1) return true;
  for (var i = 2; i < num; i++) {
    if (num % i == 0) return false;
  }
  return true;
}

function countIntegers() {
  var count = 0;
  numsArray.forEach(function (num) {
    if (Number.isInteger(num)) {
      count++;
    }
  });
  document.getElementById("result-9").innerText = `Số nguyên: ${count}`;
}

function comparePositiveAndNegativeNumbers() {
  var countPositives = 0;
  var countNegatives = 0;
  numsArray.forEach(function (num) {
    if (num > 0) {
      countPositives++;
    } else if (num < 0) {
      countNegatives++;
    }
  });
  if (countPositives > countNegatives) {
    document.getElementById("result-10").innerText = "Số dương > Số âm";
  } else if (countNegatives > countPositives) {
    document.getElementById("result-10").innerText = "Số âm > Số dương";
  } else {
    document.getElementById("result-10").innerText = "Số âm = Số dương";
  }
}

function getRndInteger(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}
